//
//  AppDelegate.h
//  Currency Ticker
//
//  Created by Igor on 6/12/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

