//
//  Currency.h
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

@import Foundation;

@interface Currency : NSObject

@property (nonatomic, copy) NSString *currencyName;
@property (nonatomic, copy) NSString *currentRate;

- (instancetype)initWithItemName:(NSString *)currencyName rate:(NSString *)currentRate;

@end
