//
//  Currency.m
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "Currency.h"

@implementation Currency

- (instancetype)initWithItemName:(NSString *)currencyName rate:(NSString *)currentRate
{
    self = [super init];
    
    if (self)
    {
        _currencyName = currencyName;
        _currentRate = currentRate;
    }
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    
    return self;
}

@end
