//
//  CurrencyDetailViewController.h
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "PNChart.h"
#import "PNChartDelegate.h"
@import UIKit;

@class Currency;

@interface CurrencyDetailViewController : UIViewController

@property (strong, nonatomic) Currency *currencyDetail;
@property (weak, nonatomic) PNLineChart *lineChart;
@property (strong, nonatomic) NSMutableDictionary *historicalRates;
@property (weak, nonatomic) IBOutlet PNLineChart *chartView;
@property (strong, nonatomic) IBOutlet UICountingLabel *countingLabel;

@end
