//
//  CurrencyDetailViewController.m
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "CurrencyDetailViewController.h"
#import "Currency.h"
#import "AFNetworking.h"
#import "Favorites.h"
#import "UICountingLabel.h"

@interface CurrencyDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rate;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *trashButton;

@end

@implementation CurrencyDetailViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)getCurrencyName
{
    NSURL *url = [NSURL URLWithString:@"https://openexchangerates.org/api/currencies.json?app_id=2cc4d248235f4c76ba99a06cee997cb7"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        // set label to currency's name
        self.nameLabel.text = [responseObject objectForKey:self.currencyDetail.currencyName];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Operation %@ failed with error: %@", operation, error);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"No internet access"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }];
    
    [operation start];
}

- (void)getDataForChart
{
    // date needed for API
    NSDateFormatter *dateAPI=[[NSDateFormatter alloc] init];
    [dateAPI setDateFormat:@"yyyy-MM-dd"];
    
    // date needed to display
    NSDateFormatter *dateToDictiorany=[[NSDateFormatter alloc] init];
    [dateToDictiorany setDateFormat:@"MMM d"];
    
    NSDate *now = [NSDate date];
    
    // collect data from last 7 days
    for (int i=6; i>0; i--) {
        
        //take date from i days ago
        NSDate *historicalDay = [now dateByAddingTimeInterval:-i*24*60*60];
        NSString *stringDate = [dateAPI stringFromDate:historicalDay];
        __block NSString *stringDateToDisplay = [dateToDictiorany stringFromDate:historicalDay];

        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://openexchangerates.org/api/historical/%@.json?app_id=7076e96a2918457a9e7ceffa337ddc18", stringDate]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // add object to dictionary with - key: data, value: currency rate
            NSDictionary *dict = [responseObject objectForKey:@"rates"];
            [self.historicalRates setObject:[dict valueForKey:self.currencyDetail.currencyName] forKey:stringDateToDisplay];
            if ([self.historicalRates count] == 6)
            {
                // add current rate
                stringDateToDisplay = [dateToDictiorany stringFromDate:now];
                [self.historicalRates setObject:self.currencyDetail.currentRate forKey:stringDateToDisplay];
                [self setupChart];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Operation %@ failed with error: %@", operation, error);
            
        }];
        
        [operation start];
    
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addToFavorites
{
    // check if this currency is already marked as favorite
    if ([[[Favorites favoritesInstance] favoritesDic] objectForKey:self.currencyDetail.currencyName] == nil)
    {
        [[Favorites favoritesInstance] addFavorite:self.currencyDetail.currencyName :self.currencyDetail.currentRate];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't add currency to favorites"
                                                        message:@"This currency is already marked as favorite."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)removeFromFavorites
{
    // check if this currency is marked as favorite
    if ([[[Favorites favoritesInstance] favoritesDic] objectForKey:self.currencyDetail.currencyName] != nil)
    {
        [[Favorites favoritesInstance] removeFavorite:self.currencyDetail.currencyName];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't remove currency from favorites"
                                                        message:@"This currency is not marked as favorite."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)setupChart
{
    // get dates in ascending order
    NSArray *keyArray = [self.historicalRates allKeys];
    NSArray *sortedKeys = [keyArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    // chart within UIView chartView
    PNLineChart * lineChart = [[PNLineChart alloc] initWithFrame:self.chartView.frame];
    
    lineChart.yLabelFormat = @"%1.3f";
    [lineChart setXLabels:sortedKeys];
    lineChart.showCoordinateAxis = YES;
    
    // Line Chart #1
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    // fill chart's data
    for (int i=0; i<7; i++) {
        
        NSString *numberInString = [self.historicalRates valueForKey:[sortedKeys objectAtIndex:i]];
        NSNumber *num = [[NSNumber alloc] initWithFloat:[numberInString floatValue]];
        [arr addObject:num];
    }
    
    NSNumber *max=[arr valueForKeyPath:@"@max.self"];
    NSNumber *min=[arr valueForKeyPath:@"@min.self"];
    
    lineChart.yFixedValueMax = 1.01 * [max doubleValue];
    lineChart.yFixedValueMin = 0.99 * [min doubleValue];
    NSArray * data01Array = arr;

    PNLineChartData *data01 = [PNLineChartData new];
    data01.color = PNTwitterColor;
    data01.alpha = 0.9f;
    data01.itemCount = lineChart.xLabels.count;
    data01.inflexionPointStyle = PNLineChartPointStyleTriangle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [[data01Array objectAtIndex:index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    
    lineChart.chartData = @[data01];
    [lineChart strokeChart];
    self.lineChart = lineChart;
    [self.view addSubview:self.lineChart];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.historicalRates = [[NSMutableDictionary alloc] init];
    self.navigationItem.title = self.currencyDetail.currencyName;
    
    // set up proper right bar button item
    if ([[[Favorites favoritesInstance] favoritesDic] objectForKey:self.currencyDetail.currencyName] != nil)
    {
        [self setAsFavorite:YES];
    }
    else
    {
        [self setAsFavorite:NO];
    }
    
    [self getCurrencyName];
    [self getDataForChart];
    
    // set up counting label
    self.countingLabel.method = UILabelCountingMethodLinear;
    self.countingLabel.textAlignment = NSTextAlignmentCenter;
    [self.countingLabel countFrom:0 to:[self.currencyDetail.currentRate floatValue] withDuration:1.5f];
    self.countingLabel.font = [UIFont fontWithName:@"Menlo" size:22];

    [self.view addSubview:self.countingLabel];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(IBAction)addFavorite:(id)sender
{
    [self addToFavorites];
    [self setAsFavorite:YES];
}

-(IBAction)removeFavorite:(id)sender
{
    [self removeFromFavorites];
    [self setAsFavorite:NO];
}

- (void)setAsFavorite:(BOOL)isFavorite
{
    // we need to change which of add/trash buttons are showing
    if ((isFavorite && !self.trashButton) || (!isFavorite && !self.addButton))
    {
        UIBarButtonItem *buttonToRemove = nil;
        UIBarButtonItem *buttonToAdd = nil;
        if (isFavorite)
        {
            buttonToRemove = self.addButton;
            self.addButton = nil;
            self.trashButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(removeFavorite:)];
            buttonToAdd = self.trashButton;
        }
        else
        {
            buttonToRemove = self.trashButton;
            self.trashButton = nil;
            self.addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFavorite:)];
            buttonToAdd = self.addButton;
        }
        
        // Get the reference to the current bar buttons
        NSMutableArray *barButtons = [[self.navigationItem rightBarButtonItems] mutableCopy];

        // Remove a button from the nav and add the other one
        if (buttonToRemove)
            [barButtons removeObject:buttonToRemove];
        if (![barButtons containsObject:buttonToAdd])
            [barButtons insertObject:buttonToAdd atIndex:0];
        
        [self.navigationItem setRightBarButtonItems:barButtons];
    }
}

@end
