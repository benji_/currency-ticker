//
//  DashboardTableViewController.h
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

@import UIKit;

@interface DashboardTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *currencies;
@property (nonatomic, strong) NSArray *currenciesKeys;

@end
