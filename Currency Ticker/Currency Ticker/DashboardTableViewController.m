//
//  DashboardTableViewController.m
//  Currency Ticker
//
//  Created by Igor on 6/14/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "DashboardTableViewController.h"
#import "CurrencyDetailViewController.h"
#import "Currency.h"
#import "AFNetworking.h"

@interface DashboardTableViewController ()

@end

@implementation DashboardTableViewController


-(void)makeCurrencyRequest
{
    
    NSURL *url = [NSURL URLWithString:@"http://openexchangerates.org/api/latest.json?app_id=2cc4d248235f4c76ba99a06cee997cb7"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Rates in NSDictionary: key - currency, value - rate
        self.currencies = [responseObject objectForKey:@"rates"];
        
        // NSArray with keys in alphabetical order
        NSArray *keyArray = [self.currencies allKeys];
        self.currenciesKeys = [keyArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
        // load data to UITableView
        [self.tableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Operation %@ failed with error: %@", operation, error);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"No internet access"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    
    [operation start];
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self makeCurrencyRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self.currencies allKeys] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrencyCell" forIndexPath:indexPath];
    
    // Cell description. textLabel - currency, detailTextLabel - rate
    id aKey = self.currenciesKeys[indexPath.row];
    NSString *currencyRate = [NSString stringWithFormat:@"%@", [self.currencies objectForKey:aKey]];
    NSString *currencyName = self.currenciesKeys[indexPath.row];
    
    cell.textLabel.text = currencyName;
    cell.detailTextLabel.text = currencyRate;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];

    if ([segue.identifier isEqualToString:@"segue"]) {
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        id aKey = self.currenciesKeys[indexPath.row];
        NSString *currencyRate = [NSString stringWithFormat:@"%@", [self.currencies objectForKey:aKey]];
        NSString *currencyName = self.currenciesKeys[indexPath.row];
        
        CurrencyDetailViewController *detailVC = [segue destinationViewController];
        detailVC.currencyDetail = [[Currency alloc] initWithItemName:currencyName rate:currencyRate];
    }
}

@end
