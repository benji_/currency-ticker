//
//  DashboardViewController.h
//  Currency Ticker
//
//  Created by Igor on 6/12/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *dol_eur;
@property (weak, nonatomic) IBOutlet UILabel *dol_eur_price;
@property (weak, nonatomic) IBOutlet UIButton *trigger;

@end
