//
//  DashboardViewController.m
//  Currency Ticker
//
//  Created by Igor on 6/12/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "DashboardViewController.h"
#import "AFNetworking.h"

@interface DashboardViewController ()

@property (strong, nonatomic) NSDictionary *currencies;
@property (strong, nonatomic) NSMutableArray *currency;

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *url = @"http://openexchangerates.org/api/latest.json?app_id=eb24b8e124df4a9796e69e39036673b9";
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:url
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"JSON: %@", responseObject);
             
             NSDictionary *letters = [responseObject objectForKey:@"rates"];
             //NSArray *lettersArray = [letters allKeys];
             
             NSLog(@"%@", letters);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             // Handle failure
         }];
}

- (IBAction)addReminder:(id)sender
{
    /*NSString *url = @"http://openexchangerates.org/api/latest.json?app_id=eb24b8e124df4a9796e69e39036673b9";
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.currencies = (NSDictionary *)responseObject;
        self.currency = self.currencies[@"rates"][@"EUR"];
        NSLog(@"JSON: %@", self.currency);
        _dol_eur_price.text = [NSString stringWithFormat:@"%@", self.currency ];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Handle error
    }];
    
    [operation start];
    
    NSLocale* curentLocale = [NSLocale currentLocale];
    
    NSLog(@"%@", [NSString stringWithFormat:
                  NSLocalizedString(@"The current locale is: %@",
                                    @"String used to display the current locale."),
                  [curentLocale displayNameForKey:NSLocaleIdentifier
                                            value:[curentLocale localeIdentifier]]]);*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
