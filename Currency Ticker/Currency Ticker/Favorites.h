//
//  Favorites.h
//  Currency Ticker
//
//  Created by Igor on 6/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

@import Foundation;

@class Currency;

@interface Favorites : NSObject <NSCoding>

@property (strong, nonatomic) NSMutableDictionary *favoritesDic;
@property (strong, nonatomic) NSMutableArray *favoritesArray;


+ (Favorites *)favoritesInstance;
- (void)addFavorite:(NSString *)favoriteName :(NSString *)favoriteRate;
- (void)removeFavorite:(NSString *)favoriteName;

- (void)save;
@end
