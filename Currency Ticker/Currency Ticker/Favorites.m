//
//  Favorites.m
//  Currency Ticker
//
//  Created by Igor on 6/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "Favorites.h"
#import "Currency.h"
#import "AFNetworking.h"

static NSString* const favDatafavoritesDic = @"favoritesDic";
static NSString* const favDatafavoritesArray = @"favoritesArray";


@implementation Favorites


- (id)init
{
    self = [super init];
    if (self)
    {
        _favoritesDic = [[NSMutableDictionary alloc] init];
        _favoritesArray = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (Favorites *)favoritesInstance
{
    static Favorites *favoritesInstance = nil;
    static dispatch_once_t onceSecurePredicate;
    dispatch_once(&onceSecurePredicate,^
                  {
                       favoritesInstance = [self loadInstance];
                  });
    
    return favoritesInstance;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
    NSData * encodedFavArray = [NSKeyedArchiver archivedDataWithRootObject:self.favoritesArray];
    NSData * encodedFavDic = [NSKeyedArchiver archivedDataWithRootObject:self.favoritesDic];
    
    [encoder encodeObject:encodedFavArray forKey: favDatafavoritesArray];
    [encoder encodeObject:encodedFavDic forKey: favDatafavoritesDic];
}

- (Favorites *)initWithCoder:(NSCoder *)decoder
{
    self = [self init];
    if (self) {
        NSData *favArray = [decoder decodeObjectForKey:favDatafavoritesArray];
        NSData *favDic = [decoder decodeObjectForKey: favDatafavoritesDic];

        _favoritesArray = (NSMutableArray*) [NSKeyedUnarchiver unarchiveObjectWithData:favArray];
        _favoritesDic = (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:favDic];
    }
    
    return self;
}

+ (NSString*)filePath
{
    static NSString* filePath = nil;
    if (!filePath) {
        filePath =
        [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
         stringByAppendingPathComponent:@"favData"];
    }
    return filePath;
}

- (void)save
{
    NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject: self];
    [encodedData writeToFile:[Favorites filePath] atomically:YES];
}

+ (instancetype)loadInstance
{
    NSData* decodedData = [NSData dataWithContentsOfFile: [Favorites filePath]];
    if (decodedData) {
        Favorites* favData = [NSKeyedUnarchiver unarchiveObjectWithData:decodedData];
        return favData;
    }
    
    return [[Favorites alloc] init];
}

- (NSMutableArray *)allKeys
{
    return self.favoritesArray;
}

- (NSMutableDictionary *)allRates
{
    return self.favoritesDic;
}

- (void)addFavorite:(NSString *)favoriteName :(NSString *)favoriteRate;
{
    [self.favoritesArray addObject:favoriteName];
    [self.favoritesDic setObject:favoriteRate forKey:favoriteName];
    
}

- (void)removeFavorite:(NSString *)favoriteName
{
    [self.favoritesArray removeObject:favoriteName];
    [self.favoritesDic removeObjectForKey:favoriteName];
}




@end
