//
//  FavoritesViewController.h
//  Currency Ticker
//
//  Created by Igor on 6/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

@import UIKit;
@import Foundation;

@interface FavoritesViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *currencies;
@property (nonatomic, strong) NSArray *currenciesKeys;


@end
