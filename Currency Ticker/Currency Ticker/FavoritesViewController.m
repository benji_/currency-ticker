//
//  FavoritesViewController.m
//  Currency Ticker
//
//  Created by Igor on 6/17/15.
//  Copyright (c) 2015 Igor. All rights reserved.
//

#import "FavoritesViewController.h"
#import "CurrencyDetailViewController.h"
#import "Currency.h"
#import "AFNetworking.h"
#import "Favorites.h"


@interface FavoritesViewController ()

@end

@implementation FavoritesViewController

-(void)makeCurrencyRequest
{
    NSURL *url = [NSURL URLWithString:@"http://openexchangerates.org/api/latest.json?app_id=2cc4d248235f4c76ba99a06cee997cb7"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //AFNetworking asynchronous url request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Rates in NSDictionary: key - currency, value - rate
        self.currencies = [responseObject objectForKey:@"rates"];

        NSString *currencySymbol = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
        
        if ([[[Favorites favoritesInstance] favoritesDic] count] == 0)
        {
            [[Favorites favoritesInstance] addFavorite:currencySymbol :[self.currencies valueForKey:currencySymbol]];
        }
        
        // load data to UITableView
        [self.tableView reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Operation %@ failed with error: %@", operation, error);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"No internet access"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }];
    
    [operation start];
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self makeCurrencyRequest];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[[Favorites favoritesInstance] favoritesArray] count];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CurrencyCell" forIndexPath:indexPath];

    NSArray *favKeys = [[Favorites favoritesInstance] favoritesArray];
    NSDictionary *favValues = [[Favorites favoritesInstance] favoritesDic];
    
    id aKey = favKeys[indexPath.row];
    NSString *currencyRate = [NSString stringWithFormat:@"%@", [favValues objectForKey:aKey]];
    NSString *currencyName = favKeys[indexPath.row];
    
    cell.textLabel.text = currencyName;
    cell.detailTextLabel.text = currencyRate;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if ([segue.identifier isEqualToString:@"segueFav"]) {
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        NSArray *favKeys = [[Favorites favoritesInstance] favoritesArray];
        NSDictionary *favValues = [[Favorites favoritesInstance] favoritesDic];
        
        id aKey = favKeys[indexPath.row];
        NSString *currencyRate = [NSString stringWithFormat:@"%@", [favValues objectForKey:aKey]];
        NSString *currencyName = favKeys[indexPath.row];
        
        CurrencyDetailViewController *detailVC = [segue destinationViewController];
        detailVC.currencyDetail = [[Currency alloc] initWithItemName:currencyName rate:currencyRate];
    }
}

@end
